import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateLinkDto } from './dto';
import { randomBytes } from 'crypto';

@Injectable()
export class LinkService {
  constructor(private prisma: PrismaService) {}

  public async getLink(linkShort: string) {
    const link = await this.prisma.link.findFirst({
      where: {
        short: linkShort,
      },
    });

    if (!link) {
      throw new NotFoundException('Link does not exist.');
    }

    return link;
  }

  public async createLink(userId: number, dto: CreateLinkDto) {
    let shortcode;
    let exists = true;

    // Check if shortcode alrdeady exists Youtube style
    while (exists) {
      shortcode = randomBytes(4).toString('hex');
      const found = await this.prisma.link.findFirst({
        where: {
          short: shortcode,
        },
      });
      if (!found) exists = false;
    }

    const link = await this.prisma.link.create({
      data: {
        ...dto,
        short: shortcode,
        userId,
      },
    });
    return link;
  }

  public async deleteLink(userId: number, linkShort: string) {
    const link = await this.prisma.link.findFirst({
      where: {
        short: linkShort,
      },
    });

    if (!link) {
      throw new NotFoundException('Link does not exist.');
    }

    if (link.userId !== userId) {
      throw new ForbiddenException('Link does not belong to you.');
    }

    await this.prisma.link.delete({
      where: {
        id: link.id,
      },
    });
  }
}
