import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
  Body,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { LinkService } from './link.service';
import { GetUser } from '../auth/decorator';
import { JwtGuard } from '../auth/guard';
import { CreateLinkDto } from './dto';

@Controller('link')
export class LinkController {
  constructor(private linkService: LinkService) {}

  @Get(':short')
  public async getLink(@Param('short') linkShort: string) {
    return this.linkService.getLink(linkShort);
  }

  @UseGuards(JwtGuard)
  @Post()
  public async createLink(
    @GetUser('id') userId: number,
    @Body() dto: CreateLinkDto,
  ) {
    return this.linkService.createLink(userId, dto);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(JwtGuard)
  @Delete(':short')
  public async deleteLink(
    @GetUser('id') userId: number,
    @Param('short') linkShort: string,
  ) {
    return this.linkService.deleteLink(userId, linkShort);
  }
}
