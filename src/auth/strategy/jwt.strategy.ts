import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    config: ConfigService,
    private prisma: PrismaService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get('JWT_SECRET'),
    });
  }

  public async validate(payload: { sub: number; email: string }) {
    const user = await this.prisma.user.findFirst({
      where: {
        id: payload.sub,
        email: payload.email,
      },
    });

    if (!user) {
      return null; // will 401
    }

    delete user.hash;
    return user; // ! this gets set on req.user
  }
}
