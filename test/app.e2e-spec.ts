import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { PrismaService } from '../src/prisma/prisma.service';
import * as pactum from 'pactum';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  beforeAll(async () => {
    // Create App
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
      }),
    );
    await app.init();

    // Clean database
    prisma = app.get(PrismaService);
    await prisma.cleanDb();

    // Set pactum baseurl
    pactum.request.setBaseUrl('http://localhost:1312');
    await app.listen(1312);
  });

  afterAll(async () => {
    await app.close();
  });

  const user = {
    email: 'testing@example.com',
    password: '123456',
  };

  const link = {
    url: 'https://google.com',
  };

  describe('Auth', () => {
    describe('Signup', () => {
      it('should fail on bad body', () => {
        return pactum
          .spec()
          .post('/auth/signup')
          .withBody({})
          .expectStatus(400);
      });
      it('should be able to sign up', () => {
        return pactum
          .spec()
          .post('/auth/signup')
          .withBody(user)
          .expectStatus(201)
          .expectBodyContains('access_token');
      });
    });
    describe('Login', () => {
      it('should fail on bad body', () => {
        return pactum.spec().post('/auth/login').withBody({}).expectStatus(400);
      });
      it('should be able to login', () => {
        return pactum
          .spec()
          .post('/auth/login')
          .withBody(user)
          .expectStatus(201)
          .expectBodyContains('access_token')
          .stores('access_token', 'access_token'); // ! store the access_token for later tests
      });
    });
  });

  describe('User', () => {
    describe('Me', () => {
      it('should fail with no token', () => {
        return pactum.spec().get('/user/me').expectStatus(401);
      });
      it('should be able to get current user', () => {
        return pactum
          .spec()
          .get('/user/me')
          .withHeaders({
            Authorization: 'Bearer $S{access_token}',
          })
          .expectStatus(200)
          .expectBodyContains(user.email);
      });
    });
  });

  describe('Link', () => {
    describe('Create', () => {
      it('should fail with no token', () => {
        return pactum.spec().post('/link').withBody(link).expectStatus(401);
      });
      it('should fail with bad body', () => {
        return pactum
          .spec()
          .post('/link')
          .withBody({})
          .withHeaders({
            Authorization: 'Bearer $S{access_token}',
          })
          .expectStatus(400);
      });
      it('should be able to create a link', () => {
        return pactum
          .spec()
          .post('/link')
          .withBody(link)
          .withHeaders({
            Authorization: 'Bearer $S{access_token}',
          })
          .expectStatus(201)
          .stores('link_short', 'short');
      });
    });
    describe('Get', () => {
      it('should fail if it doesnt exist', () => {
        return pactum.spec().get('/link/0').expectStatus(404);
      });
      it('should be able to get a link', () => {
        return pactum
          .spec()
          .get('/link/$S{link_short}')
          .expectStatus(200)
          .expectBodyContains(link.url);
      });
    });
    describe('Delete', () => {
      it('should fail with no token', () => {
        return pactum.spec().delete('/link/$S{link_short}').expectStatus(401);
      });
      it('should fail if not owned by same user', async () => {
        await pactum
          .spec()
          .post('/auth/signup')
          .withBody({
            email: 'anotheruser@example.com',
            password: '123456',
          })
          .stores('another_token', 'access_token');
        return pactum
          .spec()
          .delete('/link/$S{link_short}')
          .withHeaders({
            Authorization: 'Bearer $S{another_token}',
          })
          .expectStatus(403);
      });
      it('should be able to delete a link', () => {
        return pactum
          .spec()
          .delete('/link/$S{link_short}')
          .withHeaders({
            Authorization: 'Bearer $S{access_token}',
          })
          .expectStatus(204);
      });
    });
  });
});
