# [shortey](https://codeberg.org/juke/shortey)
a link shortening service made in NestJS with Prisma

### Deployment
Available on https://shortey.paas.juke.fr/

A quick frontend demo is available here https://codeberg.org/juke/shortey-front

### Usage
Not really any docs yet, check out `test/app.e2e-spec.ts` and `requests.http` files.

### Prisma
```bash
$ npx prisma migrate dev # Generates the migration
# Also applies a 
# $ prisma generate
# which will create the corresponding type annotations for our schema to use in TS
```

```bash
$ npx prisma studio # Explore the database on http://localhost:5555
```

```bash
$ npm run db:dev:recreate # Recreate db and apply migrations
```

## Development

To clone the repository locally:

```bash
$ git clone https://codeberg.org/juke/shortey.git
```

## Contributing

Feel free to contact via the email bellow with a patch or anything.

### Issues
Open new issues by mailing [issues+shortey@juke.fr](mailto:issues+shortey@juke.fr)

---
beep boop

